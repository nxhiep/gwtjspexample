package com.example.hiep.client;

import com.example.hiep.client.activities.AppPlaceHistoryMapper;
import com.example.hiep.client.activities.AsyncActivityManager;
import com.example.hiep.client.activities.AsyncActivityMapper;
import com.example.hiep.client.activities.ClientFactory;
import com.example.hiep.client.activities.ClientFactoryImpl;
import com.example.hiep.client.activities.SplitAppActivityMapper;
import com.example.hiep.client.activities.home.HomePlace;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;

public class GwtJspExample implements EntryPoint {
	
	private static AppPlaceHistoryMapper historyMapper;
	public static ClientFactory clientFactory = new ClientFactoryImpl();
	
	public void onModuleLoad() {
		ClientData.prepareDataService();
		initApp();
	}

	private void initApp() {
		ClientUtils.showLoadingAjax();
		SimplePanel display = new SimplePanel();
        // Start ActivityManager for the main widget with our ActivityMapper
        AsyncActivityMapper activityMapper = new SplitAppActivityMapper(clientFactory);
		AsyncActivityManager activityManager = new AsyncActivityManager(activityMapper, clientFactory.getEventBus());
		activityManager.setDisplay(display);
		
        // Start PlaceHistoryHandler with our PlaceHistoryMapper
        historyMapper = GWT.create(AppPlaceHistoryMapper.class);
        final PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
        
        historyHandler.register(clientFactory.getPlaceController(), clientFactory.getEventBus(), new HomePlace());
        historyHandler.handleCurrentHistory();
        RootPanel.get("content").add(display);
		ClientUtils.hideLoadingAjax();
	}
}
