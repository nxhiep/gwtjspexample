package com.example.hiep.client;

import java.util.List;

import com.example.hiep.shared.Song;
import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DataServiceAsync {

	void searchSongs(String searchText, String mode, String order, String cat, int page,
			AsyncCallback<List<Song>> callback);

	void updateSongs(List<Song> songs, AsyncCallback<List<Song>> callback);
}
