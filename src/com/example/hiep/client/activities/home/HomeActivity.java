package com.example.hiep.client.activities.home;

import com.example.hiep.client.ClientUtils;
import com.example.hiep.client.activities.ClientFactory;
import com.example.hiep.client.activities.basic.BasicWebActivity;
import com.example.hiep.client.activities.test.TestPlace;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class HomeActivity extends BasicWebActivity {

	private HomeView view;
	public HomeActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getHomeView();
		super.start(panel, eventBus, view);
		panel.setWidget(view);
	}
	
	@Override
	protected void bind() {
		super.bind();
		addHandlerRegistration(view.getButtonNext().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				ClientUtils.log("Click button next");
				goTo(new TestPlace());
			}
		}));
	}
	
	@Override
	protected void loadData() {
		super.loadData();
	}
}
