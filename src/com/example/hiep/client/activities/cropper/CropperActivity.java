package com.example.hiep.client.activities.cropper;

import com.example.hiep.client.activities.ClientFactory;
import com.example.hiep.client.activities.basic.BasicWebActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;

public class CropperActivity extends BasicWebActivity {

	private CropperView view;
	public CropperActivity(ClientFactory clientFactory, Place place) {
		super(clientFactory, place);
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		view = clientFactory.getCropperView();
		super.start(panel, eventBus, view);
		panel.setWidget(view);
	}
	
	@Override
	protected void bind() {
		super.bind();
	}
	
	@Override
	protected void loadData() {
		super.loadData();
	}
}
