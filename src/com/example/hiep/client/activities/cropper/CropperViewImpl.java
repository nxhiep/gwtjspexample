package com.example.hiep.client.activities.cropper;

import com.example.hiep.client.activities.basic.BasicWebViewImpl;
import com.google.code.gwt.crop.client.GWTCropper;
import com.google.code.gwt.crop.client.GWTCropperPreview;
import com.google.code.gwt.crop.client.common.Dimension;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class CropperViewImpl extends BasicWebViewImpl implements CropperView {
	private static HomeViewImplUiBinder uiBinder = GWT.create(HomeViewImplUiBinder.class);

	interface HomeViewImplUiBinder extends UiBinder<Widget, CropperViewImpl> {
	}

	@UiField
	HTMLPanel mainPanel;

	public CropperViewImpl() {
		super();
		this.layout.getContainerPanel().add(uiBinder.createAndBindUi(this));
		initStyle();
	}

	private void initStyle() {
		final GWTCropper crop = new GWTCropper("https://www.ngoaingu24h.vn/resources/images/background-course.jpg");
		crop.setAspectRatio(1);
		mainPanel.add(crop);
		Button btn = new Button();
		btn.setText("Crop me!");
		btn.addClickHandler(new ClickHandler() {

		    @Override
		    public void onClick(ClickEvent event) {
		        Window.alert("Selected area from ("
		                 + crop.getSelectionXCoordinate() + ","
		                 + crop.getSelectionYCoordinate() + ") "
		                 + " with width=" + crop.getSelectionWidth() 
		                 + " and height=" + crop.getSelectionHeight()); 
		      }

		});
		final GWTCropperPreview cropperPreview = new GWTCropperPreview(Dimension.WIDTH, 100);
		crop.registerPreviewWidget(cropperPreview);
		mainPanel.add(btn);
		mainPanel.add(cropperPreview);
	}
}
