package com.example.hiep.client.activities.basic;

import com.example.hiep.client.GwtJspExample;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;

public class BasicWebViewImpl implements BasicWebView {
	private BasicViewImplUiBinder basicUiBinder = GWT.create(BasicViewImplUiBinder.class);

	interface BasicViewImplUiBinder extends UiBinder<Widget, Layout> {

	}

	protected final Layout layout;

	public static class Layout {

		private final BasicWebViewImpl basicView;
		@UiField
		HTMLPanel mainPanel;
		@UiField
		FlowPanel containerPanel;

		public Layout(BasicWebViewImpl basicView) {
			this.basicView = basicView;
		}

		public Layout() {
			this.basicView = null;
		}

		public HTMLPanel getMainPanel() {
			return mainPanel;
		}

		public BasicWebViewImpl getBasicView() {
			return basicView;
		}

		public void refreshLayout() {

		}

		public FlowPanel getContainerPanel() {
			return containerPanel;
		}
	}

	public BasicWebViewImpl() {
		this.layout = new Layout(this);
		basicUiBinder.createAndBindUi(this.layout);
		this.layout.getMainPanel().getElement().setId("mainPanel");
	}

	@Override
	public Widget asWidget() {
		return layout.mainPanel;
	}

	@Override
	public Layout getLayout() {
		return layout;
	}

	@Override
	public void refreshView() {
	}

	@Override
	public void showLoading(boolean isShow) {

	}

	@Override
	public FlowPanel getContentPanel() {
		return null;
	}
	
	@Override
	public EventBus getEventBus(){
		return GwtJspExample.clientFactory.getEventBus();
	}
}