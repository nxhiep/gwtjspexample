package com.example.hiep.client.activities.basic;

import com.example.hiep.client.activities.basic.BasicWebViewImpl.Layout;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.web.bindery.event.shared.EventBus;

public interface BasicWebView extends IsWidget{
	void refreshView();
	void showLoading(boolean isShow);
	Layout getLayout();
	FlowPanel getContentPanel();
	EventBus getEventBus();
}
