package com.example.hiep.client.activities;

import com.example.hiep.client.activities.basic.BasicWebActivity;
import com.example.hiep.client.activities.cropper.CropperActivity;
import com.example.hiep.client.activities.cropper.CropperPlace;
import com.example.hiep.client.activities.home.HomeActivity;
import com.example.hiep.client.activities.home.HomePlace;
import com.example.hiep.client.activities.test.TestActivity;
import com.example.hiep.client.activities.test.TestPlace;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.Window;

public class SplitAppActivityMapper implements AsyncActivityMapper {
	
	private ClientFactory clientFactory;
	
	public SplitAppActivityMapper(ClientFactory clientFactory){
		this.clientFactory = clientFactory;
	}

	@Override
	public void getActivity(Place place, ActivityCallbackHandler activityCallbackHandler) {
		if(place instanceof HomePlace) {
			runAsync(activityCallbackHandler, new HomeActivity(clientFactory, place));
		}
		if(place instanceof TestPlace) {
			runAsync(activityCallbackHandler, new TestActivity(clientFactory, place));
		}
		if(place instanceof CropperPlace) {
			runAsync(activityCallbackHandler, new CropperActivity(clientFactory, place));
		}
	}
	
	private void runAsync(final ActivityCallbackHandler activityCallbackHandler, final BasicWebActivity activity) {
		GWT.runAsync(new RunAsyncCallback() {
			@Override
			public void onFailure(Throwable err) {
				Window.alert("Vui lòng tải lại trang này");
			}
			@Override
			public void onSuccess() {
				activityCallbackHandler.onRecieveActivity(activity);
			}
		});
	}
}
