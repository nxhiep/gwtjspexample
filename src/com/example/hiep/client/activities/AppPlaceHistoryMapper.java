package com.example.hiep.client.activities;

import com.example.hiep.client.activities.cropper.CropperPlace;
import com.example.hiep.client.activities.home.HomePlace;
import com.example.hiep.client.activities.test.TestPlace;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;

@WithTokenizers({})
public class AppPlaceHistoryMapper implements PlaceHistoryMapper{
	
	String delimiter = "/";

	@Override
	public Place getPlace(String token) {
		String[] tokens = token.split(delimiter, 2);
		if (tokens[0].equals("test"))
			return new TestPlace();
		if (tokens[0].equals("cropper"))
			return new CropperPlace();
		if (tokens[0].equals("home"))
			return new HomePlace();
		return null;
	}
	
	@Override
	public String getToken(Place place) {
		if(place instanceof TestPlace) {
			return "test";
		}
		if(place instanceof HomePlace) {
			return "home";
		}
		if(place instanceof CropperPlace) {
			return "cropper";
		}
		return "";
	}
}
