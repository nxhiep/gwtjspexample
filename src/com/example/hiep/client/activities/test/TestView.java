package com.example.hiep.client.activities.test;

import java.util.List;

import com.example.hiep.client.activities.basic.BasicWebView;
import com.example.hiep.shared.VideoInfo;
import com.google.gwt.event.dom.client.HasClickHandlers;

public interface TestView extends BasicWebView{
	HasClickHandlers getButtonPrev();
	void showVideos(List<VideoInfo> videoInfos);

}
