package com.example.hiep.client.activities.test;

import java.util.List;

import com.example.hiep.client.GwtJspExample;
import com.example.hiep.client.activities.basic.BasicWebViewImpl;
import com.example.hiep.client.activities.widget.VimeoPlayer;
import com.example.hiep.client.event.NextVideoEvent;
import com.example.hiep.shared.VideoInfo;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

public class TestViewImpl extends BasicWebViewImpl implements TestView {
	private static HomeViewImplUiBinder uiBinder = GWT.create(HomeViewImplUiBinder.class);
	
	interface HomeViewImplUiBinder extends UiBinder<Widget, TestViewImpl> {
	}
	
	@UiField HTMLPanel mainPanel;
	@UiField FlowPanel videoPanel, listVideoPanel;
	private Button buttonPrev = new Button("PrevPlace");
	
	public TestViewImpl() {	 
		super();
		this.layout.getContainerPanel().add(uiBinder.createAndBindUi(this));
		mainPanel.add(buttonPrev);
	}
	
	@Override
	public HasClickHandlers getButtonPrev() {
		return buttonPrev;
	}

	@Override
	public void showVideos(List<VideoInfo> videoInfos) {
		listVideoPanel.clear();
		for (final VideoInfo videoInfo : videoInfos) {
			Anchor anchor = new Anchor();
			anchor.setText(videoInfo.getTitle());
			listVideoPanel.add(anchor);
			anchor.addClickHandler(new ClickHandler() {
				
				@Override
				public void onClick(ClickEvent event) {
					getEventBus().fireEvent(new NextVideoEvent(videoInfo));
				}
			});
		}
	}
}
