package com.example.hiep.client.event;

import com.example.hiep.shared.VideoInfo;
import com.google.gwt.event.shared.GwtEvent;

public class NextVideoEvent extends GwtEvent<NextVideoEventHandler> {
	public static final Type<NextVideoEventHandler> TYPE = new Type<NextVideoEventHandler>();

	private VideoInfo videoInfo;

	public NextVideoEvent(VideoInfo videoInfo) {
		this.videoInfo = videoInfo;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<NextVideoEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(NextVideoEventHandler handler) {
		handler.onChange(this);
	}

	public VideoInfo getVideoInfo() {
		return videoInfo;
	}
	
	public void setVideoInfo(VideoInfo videoInfo) {
		this.videoInfo = videoInfo;
	}
}
