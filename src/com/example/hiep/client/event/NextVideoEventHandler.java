package com.example.hiep.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface NextVideoEventHandler extends EventHandler{
	void onChange(NextVideoEvent event);
}
