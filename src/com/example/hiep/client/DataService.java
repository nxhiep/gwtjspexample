package com.example.hiep.client;

import java.util.List;

import com.example.hiep.shared.Song;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("data")
public interface DataService extends RemoteService {

	List<Song> searchSongs(String searchText, String mode, String order, String cat, int page);

	List<Song> updateSongs(List<Song> songs);
}
