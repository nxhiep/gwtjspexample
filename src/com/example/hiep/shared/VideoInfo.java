package com.example.hiep.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;

@Entity
public class VideoInfo implements Serializable, IsSerializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id private Long id;
	private String title = Config.TEXT_EMPTY;
	private String description = Config.TEXT_EMPTY;
	private List<String> urls = new ArrayList<String>();
	private List<String> thumbnails = new ArrayList<String>();
	@Index private Long parentId = Config.NULL_ID;
	@Index private Long createDate = Config.NULL_ID;
	@Index private int status = Config.PUBLIC_STATUS;
	@Index private Long topicId = Config.NULL_ID;
	@Index private Long lastUpdate = Config.NULL_ID;
	@Index private Long lessonVideoId = Config.NULL_ID;
	private List<Long> cardIds = new ArrayList<Long>();
	private int year = 2018;
	
	@Ignore private Long lessonId = Config.NULL_ID;
	
	public VideoInfo(){
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getUrls() {
		return urls;
	}

	public List<String> getThumbnails() {
		return thumbnails;
	}
	

	public String getThumbnail() {
		if(thumbnails == null || thumbnails.isEmpty())
			return "../images/lesson/defaultVideo.jpg";
		return thumbnails.get(thumbnails.size()-1);
	}

	public Long getTopicId() {
		return topicId;
	}

	public void setUrls(List<String> urls) {
		this.urls = urls;
	}

	public void setThumbnails(List<String> thumbnails) {
		this.thumbnails = thumbnails;
	}

	public void setTopicId(Long topicId) {
		this.topicId = topicId;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Long createDate) {
		this.createDate = createDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Long> getCardIds() {
		return cardIds;
	}

	public void setCardIds(List<Long> cardIds) {
		this.cardIds = cardIds;
	}

	public Long getLessonId() {
		return lessonId;
	}

	public void setLessonId(Long lessonId) {
		this.lessonId = lessonId;
	}

	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public Long getLastUpdate() {
		return lastUpdate;
	}
	
	public Long getLessonVideoId() {
		return lessonVideoId;
	}
	
	public void setLastUpdate(Long lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public void setLessonVideoId(Long lessonVideoId) {
		this.lessonVideoId = lessonVideoId;
	}
}
