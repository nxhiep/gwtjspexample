package com.example.hiep.shared;

import java.util.List;

public class DataSong {
	private List<Song> songs = null;
	private String status = "";
	
	public DataSong(List<Song> songs, String status) {
		this.songs = songs;
		this.status = status;
	}
	
	public List<Song> getSongs() {
		return songs;
	}
	
	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}
}
