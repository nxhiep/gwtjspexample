package com.example.hiep.shared;

public interface MyAsyncCallback<T> {
	void onCallback(T result);
}
