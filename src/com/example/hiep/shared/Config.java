package com.example.hiep.shared;

public class Config {
	
	public static final String CSN_SEARCH_URL = "http://search.chiasenhac.vn/search.php?";
	public static final String CONTENT_TYPE_HTML = "text/html; charset=utf-8";
	public static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
	
	public static final String iconPlay = "<i class=\"fa fa-play\" aria-hidden=\"true\"></i>";
	public static final String iconStop = "<i class=\"fa fa-stop\" aria-hidden=\"true\"></i>";
	public static final String iconPause = "<i class=\"fa fa-pause\" aria-hidden=\"true\"></i>";
	public static final String iconDownload = "<i class=\"fa fa-download\" aria-hidden=\"true\"></i>";
	public static final String iconSearch = "<i class=\"fa fa-search\" aria-hidden=\"true\"></i>";
	
	public static final int STATUS_LOVE_LEVEL_DELETE = -1;
	public static final int STATUS_LOVE_LEVEL_0 = 0;
	public static final int STATUS_LOVE_LEVEL_1 = 1;
	public static final int STATUS_LOVE_LEVEL_2 = 2;
	public static final int STATUS_LOVE_LEVEL_3 = 3;
	public static final int STATUS_LOVE_LEVEL_4 = 4;
	public static final int STATUS_LOVE_LEVEL_5 = 5;
	
	public static final int LOGIN_FAILED = -2;
	public static final int LOGIN_ACCOUNT_NOT_EXIST = -1;
	public static final int LOGIN_ACCOUNT_EXISTED = 0;
	public static final int LOGIN_WRONG_PASSWORD = 1;
	public static final int LOGIN_SUCCESS = 2;
	public static final String ADMIN_PASSWORD = "Nxhiep1996";
	public static final String TEXT_EMPTY = "";
	public static final Long NULL_ID = -1L;
	public static final int PUBLIC_STATUS = 1;
}
