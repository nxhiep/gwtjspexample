package com.example.hiep.shared;

import java.util.ArrayList;
import java.util.List;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Song {
	@Id private Long id;
	@Index private String key;
	@Index private String name;
	@Index private String singer;// ca si
	@Index private int type;// the loai
	private String avatar; // anh music
	private String link;
	private String lyric;// loi bai hat
	@Index private String artist;// tac gia
	@Index private String quality;// chat luong
	private List<String> linkDownload = new ArrayList<String>();
	
	public Song(){
	}

	public Song(String key, String name, String singer){ 
		key = key.substring(key.indexOf("chiasenhac.vn/") + 14);
		setKey(key);
		setLink(key + ".html");
		setName(name);
		setSinger(singer);
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSinger() {
		return singer;
	}
	public void setSinger(String singer) {
		this.singer = singer;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getLyric() {
		return lyric;
	}
	public void setLyric(String lyric) {
		this.lyric = lyric;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}

	public List<String> getLinkDownload() {
		return linkDownload;
	}
	
	public void setLinkDownload(List<String> linkDownload) {
		this.linkDownload = linkDownload;
	}
	
	public boolean hasFullData(){
		return (linkDownload != null && linkDownload.size() > 0);
	}
	
	public String get128Kbps(){
		return hasFullData() ? linkDownload.get(0) : "";
	}
	
	public String get320Kbps(){
		return hasFullData() ? linkDownload.get(1) : "";
	}
	
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
}
