package com.example.hiep.server.dao;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.hiep.server.controller.ApiController;
import com.example.hiep.server.search.SearchIndexManager;
import com.example.hiep.shared.Song;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.googlecode.objectify.ObjectifyService;

public class DAO {
	static {
		ObjectifyService.register(Song.class);
	}
	
	public List<Song> searchSongs(String searchText, String mode, String order, String cat, int page, 
			HttpServletRequest request, HttpServletResponse response) {
		Results<ScoredDocument> documents = SearchIndexManager.INSTANCE.searchSongs(searchText);
		ArrayList<String> ids = new ArrayList<String>();
		for(ScoredDocument document : documents) {
			String id = document.getId();
			if(id != null && !id.isEmpty()) {
				ids.add(id);
			}
		}
		if(ids.isEmpty()) {
			return ApiController.searchSongs(searchText, mode, order, cat, page).getSongs();
		}
		return new ArrayList<Song>(ofy().load().type(Song.class).ids(ids).values());
	}

	public List<Song> updateSongs(List<Song> songs) {
		ofy().save().entities(songs).now();
		return songs;
	}
}
