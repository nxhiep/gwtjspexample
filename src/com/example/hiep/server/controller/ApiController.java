package com.example.hiep.server.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.example.hiep.server.service.DataService;
import com.example.hiep.shared.Config;
import com.example.hiep.shared.DataSong;
import com.example.hiep.shared.Song;
import com.google.gson.Gson;

@Controller
@SessionAttributes
public class ApiController {
	private static final String URL = "http://chiasenhac.vn/";

	@RequestMapping(value = "/search-songs", method = RequestMethod.POST)
	public @ResponseBody void searchSongServlet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		searchSongs(request, response);
	}
	
	@RequestMapping(value = "/get-songs-home-page", method = RequestMethod.POST)
	public @ResponseBody void getSongHomePageServlet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String json = new Gson().toJson(searchSongs("h-main4", "list-1", "text2", "spd1"));
		response.setContentType(Config.CONTENT_TYPE_JSON);
		response.getWriter().write(json);
	}

	@RequestMapping(value = "/get-song-by-key", method = RequestMethod.POST)
	public @ResponseBody void getSongByKeyServlet(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String json = new Gson().toJson(getSong(request, response));
		response.setContentType(Config.CONTENT_TYPE_JSON);
		response.getWriter().write(json);
	}

	public Map<Integer, List<Song>> searchSongs(String param1, String param2, String param3, String param4)
			throws IOException {
		Document docRoot = Jsoup.connect(URL).get();
		Elements elements = docRoot.getElementsByClass(param1);
		Map<Integer, List<Song>> mapSongs = new HashMap<Integer, List<Song>>();
		int index = 0;
		for (Element elementRoot : elements) {
			for (Element element : elementRoot.getElementsByClass(param2)) {
				try {
					Song song = new Song();
					String avatar = element.getElementsByTag("img").get(0).attr("src");
					song.setAvatar(avatar);
					Element elementSong = element.getElementsByClass(param3).get(0);
					Element elementA = elementSong.getElementsByTag("a").get(0);
					String link = elementA.attr("href");
					song.setLink(link);
					String name = elementA.text();
					song.setName(name);
					String singer = elementSong.getElementsByClass(param4).get(0).text();
					song.setSinger(singer);
					if (!mapSongs.containsKey(index)) {
						mapSongs.put(index, new ArrayList<Song>());
					}
					mapSongs.get(index).add(song);
				} catch (Exception w) {
				}
			}
			index++;
		}
		return mapSongs;
	}

	public Song getSong(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String url = request.getParameter("url");

		Document doc = Jsoup.connect(url).get();

		String urlAvatar = "";
		Iterator<Element> imgElemIt = doc.getElementsByTag("link").iterator();
		while (imgElemIt.hasNext()) {
			Element element = imgElemIt.next();
			if (element.attr("rel").contains("image_src")) {
				urlAvatar = element.attr("href");
				break;
			}
		}

		Element element = doc.getElementById("fulllyric");

		String lyrics = element.getElementsByTag("p").text();

		Song music = new Song();
		Iterator<Element> iterator = element.getElementsByTag("a").iterator();
		Element nameElement = iterator.next();
		String name = nameElement.text();
		String singer = "", composer = "";
		while (iterator.hasNext()) {
			Element elem = iterator.next();
			String validator = elem.attr("href");
			if (validator.contains("mode=artist")) {
				singer += elem.text() + ";";
			} else if (validator.contains("mode=composer")) {
				composer += elem.text() + ";";
			}
		}
		String html = doc.getElementsByTag("script").html();
		List<String> urls = new ArrayList<String>();
		urls = getLink(html);
		music.setLinkDownload(urls);

		music.setName(name);
		music.setLink(url);
		music.setSinger(singer);
		music.setArtist(composer);
		music.setLyric(lyrics);
		music.setAvatar(urlAvatar);
		return music;
	}

	private static List<String> getLink(String html) {
		List<String> strings = new ArrayList<String>();
		int offset = 0, length = 0;
		String keyStart = "decode_download_url(\"";
		String keyEnd = ".mp3\"";
		if (html.contains(".m4a\", ")) {
			keyEnd = ".m4a\"";
		} else if (html.contains(".flac\", ")) {
			keyEnd = ".flac\"";
		} else if (html.contains(".mp4\", ")) {
			keyEnd = ".mp4\"";
		}
		offset = html.indexOf(keyStart) + keyStart.length();
		length = html.indexOf(keyEnd) + keyEnd.length() - 1;
		html = html.substring(offset, length);
		for (String item : html.split("\", \"")) {
			strings.add(item);
		}
		return strings;
	}
	
	public static void searchSongs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String mode = request.getParameter("mode");
		String order = request.getParameter("order");
		String cat = request.getParameter("cat");
		int page = 1;
		if(request.getParameter("page") != null){
			try{
				page = Integer.parseInt(request.getParameter("page"));
			}catch(Exception e){
				page = 1;
			}
		}
		if(mode == null) mode = "";
		if(order == null) order = "";
		if(cat == null) cat = "";
		DataSong dataSong = searchSongs(name, mode, order, cat, page);
		if(dataSong.getStatus().equals("success")) {
			response.setContentType(Config.CONTENT_TYPE_JSON);
			response.getWriter().write(new Gson().toJson(dataSong.getSongs()));
		} else {
			response.setContentType(Config.CONTENT_TYPE_HTML);
			response.getWriter().write(dataSong.getStatus());
		}
	}
	
	public static DataSong searchSongs(String name, String mode, String order, String cat, int page) {
		Document doc;
		if(name != null){
			name = name.replaceAll(" ", "+");
			String URL = Config.CSN_SEARCH_URL + "s=" + name + "&mode=" + mode + "&order=" + order + "&cat=" + cat + "&page=" + page;
			try {
				doc = Jsoup.connect(URL).get();
				Elements elements = doc.getElementsByClass("tenbh");
				List<Song> musics = new ArrayList<Song>();
				for (Element element : elements) {
					String t1 = element.getElementsByClass("musictitle").attr("href");
					String songName = element.getElementsByClass("musictitle").text();
					String key = t1.substring(0, t1.length() - 5);
					String songSinger = element.getElementsByTag("p").eq(1).text();
					Song music = new Song(key, songName, songSinger);
					musics.add(music);
				}
				new DataService().updateSongs(musics);
				return new DataSong(musics, "success");
			} catch (IOException e) {
				e.printStackTrace();
				return new DataSong(null, "ERROR 404: get data feild");
			}
		}else{
			return new DataSong(null, "ERROR 404: name is empty");
		}
	}
}
