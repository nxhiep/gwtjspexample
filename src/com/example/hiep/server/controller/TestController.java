package com.example.hiep.server.controller;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes
@RequestMapping(value = {TestController.URL_MAPPING, "/"})
public class TestController {
	public static final String URL_MAPPING = "/";
	
	@RequestMapping(method = RequestMethod.GET)
	public String loadHomeData(Locale locale, Model model, HttpServletRequest request, HttpServletResponse response) {
		return "home";
	}
	
	@RequestMapping(value="/test", method = RequestMethod.GET)
	public String classes(Model model, HttpServletResponse response, HttpServletRequest request) throws IOException {
		return "test";
	}
}
