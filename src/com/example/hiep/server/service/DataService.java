package com.example.hiep.server.service;

import java.util.List;

import com.example.hiep.server.dao.DAO;
import com.example.hiep.shared.Song;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class DataService extends RemoteServiceServlet implements com.example.hiep.client.DataService {

	@Override
	public List<Song> searchSongs(String searchText, String mode, String order, String cat, int page) {
		return new DAO().searchSongs(searchText, mode, order, cat, page, this.getThreadLocalRequest(), this.getThreadLocalResponse());
	}
	
	@Override
	public List<Song> updateSongs(List<Song> songs) {
		return new DAO().updateSongs(songs);
	}
}
