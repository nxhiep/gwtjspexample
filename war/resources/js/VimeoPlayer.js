var iframePanel = null;
var vimeoPlayer = null;

function initVideo(parentElement, id){
	iframePanel = $('<iframe></iframe>');
	iframePanel.attr('frameborder', '0');
	iframePanel.attr('webkitallowfullscreen');
	iframePanel.attr('mozallowfullscreen');
	iframePanel.attr('allowfullscreen');
	iframePanel.css('width', '100%');
	iframePanel.css('height', '100%');
	iframePanel.attr('id', id);
	$(parentElement).empty();
	$(parentElement).append(iframePanel);
	console.log('initVideo parentElement', parentElement, 'iframePanel', iframePanel, 'id', id);
}

function loadedUrl(url, callback){
	console.log('loadedUrl', url);
	if(!!iframePanel){
		iframePanel.attr('src', url);
		setTimeout(function(){
		vimeoPlayer = new Vimeo.Player(iframePanel);
		vimeoPlayer.on('play', function() {
	        console.log('played the video!');
	    });
	}, 1000);
	}
}