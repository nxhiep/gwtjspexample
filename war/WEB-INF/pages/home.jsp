<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	
<html>
<head>
	<title>Home</title>
	<meta charset="UTF-8">
   	<meta name="format-detection" content="telephone=no" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" />
	<meta name="msapplication-tap-highlight" content="no" />
	<meta http-equiv="content-language" content="vi" />
	<script text="text/javascript" src="../../gwtjspexample/gwtjspexample.nocache.js"></script>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	<script src="../../resources/js/VimeoPlayer.js"></script>
<!-- 	<script src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script> -->
	<script src="https://player.vimeo.com/api/player.js"></script>
	<style>
		body {
			margin: 0;
		}
	</style>
</head>
<body id="content">
</body>
</html>